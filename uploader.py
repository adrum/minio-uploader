import os
import logging
from minio import Minio

# Configure logging
log_enabled = os.environ.get('LOG_ENABLED', 'true').lower() == 'true'
logging.basicConfig(level=logging.INFO if log_enabled else logging.ERROR)

def upload_images_recursive(minio_client, bucket_name, local_directory, valid_extensions=[]):
    logging.info(f"Scanning directory: {local_directory}")

    for root, dirs, files in os.walk(local_directory):
        for file in files:
            file_path = os.path.join(root, file)
            print(file_path)
            object_name = os.path.relpath(file_path, local_directory)
            logging.info(f"Processing file: {file_path}")

            if any(file.lower().endswith(ext.lower()) for ext in valid_extensions):
                try:
                    # Upload the image to MinIO
                    minio_client.fput_object(bucket_name, object_name, file_path)
                    logging.info(f"Uploaded: {file_path} to {bucket_name}/{object_name}")
                except Exception as e:
                    logging.error(f"Error uploading {file_path}: {e}")
            else:
                logging.info(f"Skipping file: {file_path} (Invalid extension)")

    # Log the list of all files in the directory for reference
    all_files = [os.path.join(root, file) for root, _, files in os.walk(local_directory) for file in files]
    logging.info(f"All files in directory: {all_files}")


if __name__ == "__main__":
    # MinIO server information
    minio_endpoint = os.environ.get('MINIO_ENDPOINT', 'your-minio-endpoint:9000')
    minio_access_key = os.environ.get('MINIO_ACCESS_KEY', 'your-access-key')
    minio_secret_key = os.environ.get('MINIO_SECRET_KEY', 'your-secret-key')
    minio_bucket_name = os.environ.get('MINIO_BUCKET_NAME', 'your-bucket-name')

    # Local directory containing images
    local_directory = '/app/images'

    # List of valid file extensions for images (e.g., ['.jpg', '.png'])
    valid_extensions = os.environ.get('VALID_EXTENSIONS', '.jpg,.png').split(',')

    # Secure option (True or False)
    secure_option = os.environ.get('SECURE_OPTION', 'False').lower() == 'true'

    # Initialize MinIO client
    minio_client = Minio(minio_endpoint,
                         access_key=minio_access_key,
                         secret_key=minio_secret_key,
                         secure=secure_option)  # Set to True if using HTTPS

    # Upload images recursively
    upload_images_recursive(minio_client, minio_bucket_name, local_directory, valid_extensions)


    # List objects in the bucket to verify the upload
    try:
        objects = list(minio_client.list_objects(minio_bucket_name))
        logging.info(f"Objects in {minio_bucket_name}:")
        for obj in objects:
            logging.info(f"  - Name: {obj.object_name}, Last Modified: {obj.last_modified}")
    except Exception as e:
        logging.error(f"Error listing objects in {minio_bucket_name}: {e}")


