# Minio Importer

MAINTAINER Alex Drummer <drummerroma@gmail.com>


- Build the image

```bash
docker build -t my-minio-uploader .
```


- Run the image

```bash
docker run -e MINIO_ENDPOINT=your-minio-endpoint -e MINIO_ACCESS_KEY=your-access-key -e MINIO_SECRET_KEY=your-secret-key -e MINIO_BUCKET_NAME=your-bucket-name -e SECURE_OPTION='true' -e VALID_EXTENSIONS='.jpg,.png,.jpeg' -e LOG_ENABLED='true' -v $(pwd)/images:/app/images my-minio-uploader
```
Environment variables:
- MINIO_ENDPOINT
- MINIO_ACCESS_KEY
- MINIO_SECRET_KEY
- MINIO_BUCKET_NAME
- SECURE_OPTION
- FILE_EXTENSION
- LOG_ENABLED


Important: Mount your local host machines image directory as volume in the container as /app/images.
In this example:

    -v $(pwd)/images:/app/images 
mounts the local directory ./images into the container at /app/images.

    $(pwd) 
gets the current working directory, and you can replace it with the absolute path to your local directory.

The VALID_EXTENSIONS environment variable is expected to be a comma-separated list of valid file extensions (e.g., .jpg,.png,.jp2).

SECURE_OPTION environment variable is used to set the secure option for the MinIO client. It defaults to False if not specified or if set to any value other than the string 'true'. Adjust the environment variables accordingly when running the script.

LOG_ENABLED environment variable is used to control whether logging is enabled. If LOG_ENABLED is set to 'true', logging is set to INFO; otherwise, it is set to ERROR. Adjust the environment variable according to your needs when running the script.

Make sure your script (uploader.py) and the  requirements.txt is in the same directory as the Dockerfile.
