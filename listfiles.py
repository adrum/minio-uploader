import os
import logging

#log_enabled = os.environ.get('LOG_ENABLED', 'true').lower() == 'true'
log_enabled = 'true'
logging.basicConfig(level=logging.INFO if log_enabled else logging.ERROR)


def list_files_recursively(directory):
    for root, dirs, files in os.walk(directory):
        logging.info(f"Files: {files}")
        for file in files:
            file_path = os.path.join(root, file)
            logging.info(f"File: {file_path}")
            print(file_path)

# Example usage:
directory_path = "/home/alex/converter/files/storage/00000001"
list_files_recursively(directory_path)
